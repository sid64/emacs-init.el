;; Disable the toolbar and all the fancy stuff

(setq inhibit-startup-message t)

(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room

(menu-bar-mode -1)            ; Disable the menu bar

;; Use the font called Fira code retina
(set-face-attribute 'default nil :font "Fira Code Retina" :height 120)

;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Enable line number

(column-number-mode)
(global-display-line-numbers-mode t)

;; Disable line numbers for some modes
(dolist (mode '(org-mode-hook
		 term-mode-hook
		 shell-mode-hook
		 eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Add custom.el
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

; Add custom theme directory
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")

;; Initialize the package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
    (package-refresh-contents))

  ;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
    (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package auto-package-update
	     :custom
	     (auto-package-update-interval 7)
	     (auto-package-update-prompt-before-update t)
	     (auto-package-update-hide-results t)
	     :config
	     (auto-package-update-maybe)
	     (auto-package-update-at-time "09:00"))

; Vertical splitting of pop up windows
(setq split-width-threshold 0)

; If you want horizontal splitting, then
;(setq split-width-threshold 9999)

; Open windows side by side
(defun split-horizontally-not-vertically ()
  "If there's only one window (excluding any possibly active minibuffer), then
  split the current window horizontally."
  (interactive)
  (if (= (length (window-list nil 'dont-include-minibuffer-even-if-active)) 1)
    (split-window-horizontally)))
     (add-hook 'temp-buffer-setup-hook 'split-horizontally-not-vertically)

; Stop cursor blinking
(blink-cursor-mode 0)

; Enable python mode
(autoload 'python-mode "python-mode.el" "Python mode." t)

;; PACKAGES
(use-package command-log-mode)


(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
:config
  (ivy-mode 1))
(use-package counsel)


(use-package rainbow-mode
  :defer t
  :hook (org-mode
	 python-mode-hook
	 emacs-lisp-mode))

(use-package anaconda-mode)
(add-hook 'python-mode-hook 'anaconda-mode)
(add-hook 'python-mode-hook 'anaconda-eldoc-mode)

(use-package org-alert
  :ensure t)
(setq alert-default-style 'libnotify)

(use-package xcode-mode)

(use-package alert
  :commands alert
  :config
  (setq alert-default-style 'notifications))

(use-package perspective
  :bind
  ("C-x C-b" . persp-list-buffers)   ; or use a nicer switcher, see below
  :config
  (persp-mode))

  ; Programming languages
(use-package markdown-mode)
(use-package lua-mode)
(use-package haskell-mode)


; Use the all-the-icons package for things like doom-modeline
(use-package all-the-icons)


;; Treemacs
(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                5000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-evil
  :after (treemacs evil)
  :ensure t)

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)

(use-package treemacs-persp ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs persp-mode) ;;or perspective vs. persp-mode
  :ensure t
  :config (treemacs-set-scope-type 'Perspectives))


; Use the projectile package

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Projects/")
    (setq projectile-project-search-path '("~/Projects/")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))


; Elfeed rss and atom feed reader
(use-package elfeed
  :config
  (setq elfeed-search-feed-face ":foreground #fff :weight bold"
	elfeed-feeds (quote
		      (("https://www.reddit.com/r/linux.rss" reddit linux)
		       ("https://www.reddit.com/r/LinuxUsersGroup.rss" reddit Linuxusergroup)
                       ("https://itsfoss.com/feed/" itsfoss linux)
		       ("https://news.itsfoss.com/feed/" itsfoss news)
		       ("https://news.itsfoss.com/web-stories/feed/" itsfoss webstories)
		       ("https://karl-voit.at/feeds/lazyblorg-all.atom_1.0.links-only.xml" karl-voit)
		       ("https://www.fosslinux.com/feed" fosslinux)
		       ("https://www.cyberciti.com/feed/" linux cyberciti)
		       ("https://lucidmanager.org/index.xml" lucid-manager emacs)
                       ("https://www.zdnet.com/topic/linux/rss.xml" zdnet linux)
		       ("https://www.linuxtoday.com/biglt.rss" linuxtoday)
		       ("https://nyxt.atlas.engineer/feed" nyxt browser)
		       ("https://www.fsf.org/news/rss" freesoftwarefoundation news)
		       ("https://www.fsf.org/blogs/rss" freesoftwarefoundation blog)
		       ("https://omgubuntu.co.uk/feed" omg! ubuntu)
		       ("https://openforeveryone.net/feed" openforeveryone)
		       ("https://www.reddit.com/r/orgmode.rss" reddit org-mode)
		       ("http://xahlee.info/emacs/emacs/blog.xml" xahlee emacs blog)
		       ("https://updates.orgmode.org/feed/updates" orgmode updates)
		       ("https://endeavouros.com/feed" endeavouros)
		       ("https://linuxscoop.tumblr.com/rss" linuxscoop)
                       ("https://betanews.com/feed" betanews linux)
		       ("https://www.reddit.com/r/opensource.rss" reddit opensource)
		       ("https://www.makeuseof.com/feed/category/linux/" makeusof linux)
		       ("https://www.tecmint.com/feed/" tecmint)
		       ("https://www.fosslinux.com/feed" fosslinux)
                       ("https://opensource.com/feed" opensource website)
		       ("https://www.anytype.blog/rss/" anytype blog)
               ("https://distrowatch.com/news/dwd.xml" distrowatch linux)))))


; eww(Emacs web browser)
(setq
 browse-url-browser-function 'eww-browse-url ; Use eww as the default browser
 shr-use-fonts  nil                          ; No special fonts
 shr-use-colors nil                          ; No colours
 shr-indentation 2                           ; Left-side margin
 shr-width 70)                               ; Fold text to 70 columns
;eww-search-prefix "https://wiby.me/?q=")    ; Use another engine for searching

;Magit
(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

;; Configure org mode
(defun efs/org-font-setup ()
 ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•")))))))

(with-eval-after-load 'org
  ;; This is needed as of Org 9.2
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python")))


 ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (with-eval-after-load 'org-block ...
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))


(use-package org-download)
(add-hook 'dired-mode-hook 'org-download-enable)


; Org tags
(setq org-tag-alist
      '((startgroup)

        (:endgroup)
        ("@home" . ?H)
        ("@work" . ?W)
        ("note" . ?n)
	("idea" . ?i)))

(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

; Use the org-mode
(use-package org
  :hook (org-mode . efs/org-mode-setup)
  :config
  (setq org-ellipsis " ▾"))



; EVIL

;; Use evil mode for vim keybindings
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

; Use dmenu inside emacs
(use-package dmenu
  :ensure t)

; Highlight current line
(when window-system (add-hook 'prog-mode-hook 'hl-line-mode))

; Enable toc or table of contents
(use-package toc-org
  :commands toc-org-enable
  :init (add-hook 'org-mode-hook 'toc-org-enable))

(use-package ox-man
  :ensure nil)


; Make the text appear in the middle
(defun dw/org-mode-visual-fill ()
  (setq visual-fill-column-width 110
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :defer t
  :hook (org-mode . dw/org-mode-visual-fill))

(use-package org-drill
  :ensure t)

(use-package org-tree-slide
  :custom
  (org-image-actual-width nil))

(use-package web-mode
  :mode "(\\.\\(html?\\|ejs\\|tsx\\|jsx\\)\\'"
  :config
  (setq-default web-mode-code-indent-offset 2)
  (setq-default web-mode-markup-indent-offset 2)
  (setq-default web-mode-attribute-indent-offset 2))


;; 1. Start the server with `httpd-start'
;; 2. Use `impatient-mode' on any buffer
  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

(setq org-agenda-files
      '("~/org/Tasks.org"))

(setq org-modules
    '(org-crypt
        org-habit))

 (setq org-todo-keywords
    '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
      (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")))

; Use the built in timer for org mode
(setq org-clock-sound "~/.emacs.d/notification.wav")

;; Change bullets to something nice in org
  (use-package org-superstar
      :config
      (setq org-superstar-special-todo-items t)
      (add-hook 'org-mode-hook (lambda ()
                                 (org-superstar-mode 1))))

; Use the package called dashboard,  which is a really cool startup screen
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-banner-logo-title "The ultimate productivity tool!")         ;Set Title
(setq dashboard-init-info "EMACS")

(setq dashboard-set-file-icons t)
(dashboard-modify-heading-icons '((recents . "file-text")
                                  (bookmarks . "book"))))

(setq dashboard-startup-banner "~/.emacs.d/dashLogo.png")
(setq dashboard-items '((recents . 5)
			(bookmarks . 5)
                          (agenda . 3 )
                          (projects . 3)))

 ;Use dashboard when typing emacsclient
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))



(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :hook (lsp-mode . efs/lsp-mode-setup)
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  :config
  (lsp-enable-which-key-integration t))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'bottom))

(use-package lsp-treemacs
  :after lsp)

(use-package lsp-ivy
  :after lsp)

; Doom modeline is a better panel

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 10)))

;; Enable themes
(use-package doom-themes
   :init (load-theme 'doom-material-dark t))


(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 1))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package general
  :config
  (general-create-definer rune/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")

  (rune/leader-keys
    "t"  '(:ignore t :which-key "toggles")
    "tt" '(counsel-load-theme :which-key "choose theme")
    "b"  '(bookmark-jump :which-key)
    "sb" '(counsel-switch-buffer :which-key)
    "kb" '(kill-buffer :which-key)
    "d"  '(dmenu :which-key)
    "f"  '(counsel-find-file :which-key)
    "wc" '(evil-window-delete :which-key)
    "sw" '(other-window :which-key)
    "tm" '(treemacs :which-key)
    "ot" '(org-todo-list :which-key)))


; to remove the ^ when typing M-x
(setq ivy-initial-inputs-alist nil)

;Use the package called smex that remembers the previous command you typed after typing M-x, so it will show last command first
(use-package smex)
(smex-initialize)


 ; Use company(this is autocompletion
(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))
(add-hook 'after-init-hook 'global-company-mode)

(use-package company-box
             :hook (company-mode . company-box-mode))


; Elpy emacs python environment
(use-package elpy :ensure t
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable))

; parenthesis autocompletion
(use-package paredit)

; Disable backups, type 't' instead of 'nil' if you want backups
(setq make-backup-files nil)

(use-package flycheck)


; Org ref
(use-package org-ref)

; Writeroom mode
(use-package writeroom-mode)

(require 'org-faces
	 ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Manrope" :weight 'regular :height (cdr face))))


; Pdf tools
(use-package pdf-tools)

; pomodoro support in org-mode
(use-package org-pomodoro
  :ensure t)

; Muse package
(use-package muse)
(add-to-list 'load-path "~/Muse/")
     
     (require 'muse-mode)     ; load authoring mode
     
     (require 'muse-html)     ; load publishing styles I use
     (require 'muse-latex)
     (require 'muse-texinfo)
     (require 'muse-docbook)
     
     (require 'muse-project)  ; publish files in projects

; Evil nerd commenter
(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))
